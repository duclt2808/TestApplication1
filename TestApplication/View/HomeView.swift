//
//  HomeView.swift
//  TestApplication
//
//  Created by Duc Le on 5/30/23.
//

import SwiftUI

struct HomeView: View {
    var body: some View {
        VStack() {
            Image("error")
                .resizable()
                .frame(width: 345, height: 245)
                .padding(20)
            Text("Connection errors")
                .fontWeight(.bold)
                .font(.title)
                .foregroundColor(.black)
                .padding(.top, 20)
            Text("The network connection is unavailable or unstable. Please check and try again")
                .font(.headline)
                .foregroundColor(.gray)
                .padding(10)
            Button {
                print("Edit button was tapped")
            } label: {
                Label("Refresh", systemImage: "square.and.arrow.down.on.square.fill")
            }
            .frame(maxWidth: 340, maxHeight: 40)
            .padding(10)
            .background(Color(red: 46 / 255, green: 196 / 255, blue: 182 / 255))
            .foregroundColor(.white)
            .font(.title2)
            .cornerRadius(8)
            .padding(.top, 20)
            
        }
        .padding(.bottom, 50)
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
