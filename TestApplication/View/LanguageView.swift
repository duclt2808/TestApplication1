//
//  LanguageView.swift
//  TestApplication
//
//  Created by Duc Le on 5/31/23.
//

import SwiftUI

struct LanguageView: View {
    @State var isChecked:Bool = false
    func toggle(){isChecked = !isChecked}
    
    var body: some View {
        VStack() {
            HStack {
                Button {
                    
                } label: {
                     Image(systemName: "arrowshape.turn.up.backward")
                        .foregroundColor(.black)
                        
                }
                Spacer()
                Text("language")
                    .font(.title2)
                    .foregroundColor(.gray)
                    .padding(.trailing, 135)
            }
            .padding(.horizontal)
          
            Divider()
            HStack {
                Image("united states")
                Text("United States")
                    .font(.title3)
                    .foregroundColor(.white)
                    .padding(.leading, 10)
                Spacer()
                Divider()
                    .frame(height: 50)
                Button(action: toggle){
                            HStack{
                                Image(systemName: isChecked ? "checkmark.circle": "circle")
                                    .foregroundColor(.white)
                                    .frame(width: 50, height: 50)
                            }
                        }
                    
            }
            .padding(10)
            .background(Color(red: 46 / 255, green: 196 / 255, blue: 182 / 255))
            .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
            .frame(maxWidth: 350, maxHeight: 100)
            .cornerRadius(10)
            
            HStack {
                Image("vietnam")
                Text("Viet Nam")
                    .font(.title3)
                    .foregroundColor(.black)
                    .padding(.leading, 10)
                Spacer()
                Divider()
                    .frame(height: 50)
                Button(action: toggle){
                            HStack{
                                Image(systemName: isChecked ? "checkmark.circle": "circle")
                                    .foregroundColor(.black)
                                    .frame(width: 50, height: 50)
                            }
                        }
                    
            }
            .padding(10)
            .background(Color.white)
            .clipShape(RoundedRectangle(cornerRadius: 8, style: .continuous))
            .shadow(color: .gray, radius: 0.5)
            .frame(maxWidth: 350, maxHeight: 100)
            .cornerRadius(10)
            Spacer()
            }
    }
}

struct LanguageView_Previews: PreviewProvider {
    static var previews: some View {
        LanguageView()
    }
}
