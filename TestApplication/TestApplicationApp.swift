//
//  TestApplicationApp.swift
//  TestApplication
//
//  Created by Duc Le on 5/30/23.
//

import SwiftUI

@main
struct TestApplicationApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
